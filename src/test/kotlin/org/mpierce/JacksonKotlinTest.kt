package org.mpierce

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class JacksonKotlinTest {
    @Test
    internal fun serializeWithSuperclassFields() {
        val mapper = ObjectMapper().apply { registerModule(KotlinModule()) }

        assertEquals("""{"annotationName":"asdf"}""",
                mapper.writer().writeValueAsString(ChildClass("asdf")))
    }

    @Test
    internal fun serializeWithoutSuperclassFields() {
        val mapper = ObjectMapper().apply { registerModule(KotlinModule()) }

        assertEquals("""{"annotationName":"asdf"}""",
                mapper.writer().writeValueAsString(NoSuperclass("asdf")))
    }
}

open class SuperClass(@JsonProperty("annotationName") val someCrazyFieldName: String)
class ChildClass(s: String) : SuperClass(s)

class NoSuperclass(@JsonProperty("annotationName") val someCrazyFieldName: String)
