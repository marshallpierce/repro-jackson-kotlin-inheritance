Repro of a bug where Jackson's `@JsonProperty` is not respected on superclass fields.

Run `./gradlew test` or run the test in your preferred IDE.
